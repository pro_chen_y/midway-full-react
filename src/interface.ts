import '@midwayjs/core';
import type { SwaggerOptions } from '@midwayjs/swagger';

import { InlineConfig } from 'vite';
import * as serve from 'koa-static';

import { koaCompress } from './types/koa-compress.js';

export namespace vitessr {
  /**
   * @description Vite SSR 开发模式配置
   */
  export interface DevOptions {
    /**
     * 源码目录，相对于 {root}
     */
    src?: string;
    /**
     * vite 调试服务选项
     */
    devServerOpts?: DevServerOptions;
  }

  /**
   * @description Vite SSR 生产模式配置
   */
  export interface ProdOptions {
    /**
     * 编译输出目录，默认 dist， 路径：{root}/{dist}
     */
    dist?: string;
    /**
     * 客户端构建输出目录 默认 client，路径：{root}/{dist}/{client}
     */
    client?: string;
    /**
     * 服务端构建输出目录 默认 server，路径：{root}/{dist}/{server}
     */
    server?: string;
  }

  export type DevServerOptions = InlineConfig;

  /**
   * @description Vite SSR 生产模式下资源文件映射配置 (koa-static)
   */
  export type StaticOptions = serve.Options;

  /**
   * @description Vite SSR 配置 (ServerSideRender-service)
   */
  export interface Options {
    /**
     * 禁止启动前端服务
     */
    disabled?: boolean;
    /**
     * vite ssr 项目访问路径 默认 '/'
     */
    prefix?: string;
    /**
     * vite ssr 项目目录 默认 'web'，相对于 appDir
     */
    root?: string;
    /**
     *  SSR 生产配置
     */
    prod?: ProdOptions;
    /**
     *  SSR 开发配置
     */
    dev?: DevOptions;
    /**
     * 静态文件中间件选项
     */
    staticOpts?: StaticOptions;
    /**
     * 通信压缩选项
     */
    compressOptions?: koaCompress.Options;
  }
}

/**
 * @description OpenAPI 客户端代码生成配置 (ClientAPIGenerator-service)
 */
export interface ClientApiOptions {
  /**
   * 生成代码的执行文件
   */
  bin?: string;
  /**
   * openapi 文档根路径
   */
  prefix?: string;
  /**
   * 文件名称
   */
  document?: string;
  /**
   * 生成器键值 (openapitools.json)
   */
  generatorKey?: string;
  /**
   * openapi 文档缩进空格数量
   */
  spaces?: number;
  /**
   * 是否允许生成 (默认在 development 模式下启动)
   */
  enable?: boolean;
}

// 重定义 Midwayjs 配置
declare module '@midwayjs/core/dist/interface.js' {
  interface MidwayConfig {
    vitessr?: vitessr.Options;

    clientApi?: ClientApiOptions;

    swagger?: SwaggerOptions;
  }
}

/**
 * @description User-Service parameters
 */
export interface IUserOptions {
  userId: string;
}
