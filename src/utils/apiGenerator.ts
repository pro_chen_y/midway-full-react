export { apiGenCode };

import { spawn } from 'node:child_process';
import { existsSync, readdirSync, writeFileSync, mkdirSync } from 'node:fs';
import { join } from 'node:path';

const COLOR_RESET = '\x1b[0m';
const COLOR_RED = '\x1b[31m';
const COLOR_GREEN = '\x1b[32m';
const COLOR_BLUE = '\x1b[34m';

function generateApis(apidir: string, genkey: string) {
  let _apidir = '';
  if (apidir.endsWith('docs') || apidir.endsWith('docs/')) {
    _apidir = join(apidir, '..', genkey);
  } else {
    _apidir = join(apidir, genkey);
  }

  const dir = join(process.cwd(), _apidir);
  const root = join(
    process.cwd(),
    _apidir.replace(/^[.]*[\\/]/, '').split(/[\\|/]/)[0]
  );

  if (existsSync(dir)) {
    const files = readdirSync(dir, { withFileTypes: true }) || [];
    files
      .filter(d => d.isDirectory())
      .forEach(d => {
        const importPath = join(d.path, d.name)
          .replace(root, '~')
          .replace(/\\/g, '/');
        const output = join(
          root,
          'apis',
          d.name === 'apistore' ? '.apistore.ts' : `${d.name}.apistore.ts`
        );
        writeFileSync(
          output,
          `export * from '${importPath}'\n\nexport * from './openapitools'\n`
        );
        console.log(`${COLOR_GREEN}apis: ${output}${COLOR_RESET}`);
      });
  } else {
    throw new Error('API directory not found');
  }
}

function verifydirs(apidir: string) {
  const _apidir = join(process.cwd(), apidir);
  if (!existsSync(_apidir)) {
    mkdirSync(_apidir);
  }

  const apidocs = join(_apidir, 'docs');
  if (!existsSync(apidocs)) {
    mkdirSync(apidocs);
  }

  const apistorage = join(_apidir, 'storage');
  if (!existsSync(apistorage)) {
    mkdirSync(apistorage);
  }
}

function apiGenCode(
  apidir: string,
  genkey: string,
  printLogs: boolean = false
): Promise<boolean> {
  verifydirs(apidir);

  const args = ['generate', '--generator-key', genkey];

  return new Promise(resolve => {
    const cp = spawn('openapi-generator-cli', args, {
      shell: true,
      cwd: process.cwd(),
    });

    if (printLogs) {
      cp.stdout.on('data', chunk => {
        process.stdout.write(chunk);
      });
    }

    cp.on('exit', (code /*, signal*/) => {
      if (code !== 0) {
        console.error(
          `${COLOR_RED}Please check the parameters or try to install openapi-generator-cli.`,
          `\n${COLOR_BLUE}Example: npm i -g openapi-generator-cli`,
          `\nReference: https://openapi-generator.tech/docs/installation${COLOR_RESET}`
        );
        resolve(false);
      } else {
        console.info(
          `${COLOR_GREEN}Client API generated, path: ${apidir}/${genkey}${COLOR_RESET}`
        );

        try {
          generateApis(apidir, genkey);
          resolve(true);
        } catch (e) {
          console.error(`${COLOR_RED}${e.stack}${COLOR_RESET}`);
          resolve(false);
        }
      }
    });
  });
}
