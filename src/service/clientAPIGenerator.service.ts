import {
  App,
  Singleton,
  Provide,
  Config,
  Inject,
  MidwayConfigService,
  Logger,
} from '@midwayjs/core';
import { ILogger } from '@midwayjs/logger';
import { Application } from '@midwayjs/koa';
import { SwaggerExplorer } from '@midwayjs/swagger';
import { writeFileSync } from 'node:fs';
import { resolve } from 'node:path';
import { apiGenCode } from '../utils/apiGenerator.js';
import { ClientApiOptions } from '../interface.js';

@Provide()
@Singleton()
export class ClientAPIGenerator extends SwaggerExplorer {
  @Config('clientApi')
  private clientApiConfig: ClientApiOptions;

  @App()
  private app: Application;

  @Inject()
  private configService: MidwayConfigService;

  @Logger()
  private logger: ILogger;

  async generate() {
    if (this.clientApiConfig?.enable) {
      let globalPrefix =
        this.configService.getConfiguration('globalPrefix') ||
        this.configService.getConfiguration('koa.globalPrefix');

      if (globalPrefix) {
        if (!/^\//.test(globalPrefix)) {
          globalPrefix = '/' + globalPrefix;
        }

        this.addGlobalPrefix(globalPrefix);
      }

      this.scanApp();

      const {
        prefix = 'web/.api/docs',
        generatorKey,
        document = 'apistore',
        spaces = 2,
      } = this.clientApiConfig ?? {};

      // 生成 openapi 3.x 文档 => 'web/.api/docs/apistore.json'
      const apiFilePath = resolve(this.app.getAppDir(), prefix, document);
      writeFileSync(
        apiFilePath,
        JSON.stringify(this.getData(), null, spaces),
        'utf-8'
      );

      this.logger.info(`written to file ${apiFilePath}`);

      // 生成 openapi 客户端代码
      await apiGenCode(prefix, generatorKey, true);
    }
  }
}
