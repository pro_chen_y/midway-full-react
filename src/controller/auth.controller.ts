import { Config, Inject, Controller, Post, Body } from '@midwayjs/core';
import { Context } from '@midwayjs/koa';
import { UserService } from '../service/user.service.js';
import { LocalPassportMiddleware } from '../middleware/local.middleware.js';
import { ApiBody, ApiHeader, ApiResponse } from '@midwayjs/swagger';
import { LoginFormDTO, UserInfoDTO, NormalDTO } from '../dto/index.js';

@ApiHeader({ name: 'x-csrf-token', description: 'csrfToken' })
@Controller('/auth', {
  tagName: 'auth',
  description: 'local authentication controller',
})
export class AuthController {
  @Inject()
  ctx: Context;

  @Config('passport')
  passConfig: any;

  @Inject()
  userService: UserService;

  @Post('/login')
  @ApiBody({ description: 'login form' })
  @ApiResponse({
    status: 200,
    type: NormalDTO,
  })
  async login(@Body() userDto: LoginFormDTO) {
    const { username, password } = userDto;
    const { sessionUserProperty, userProperty } = this.passConfig;
    delete this.ctx.session[sessionUserProperty];

    if (true === (await this.userService.verifyUser(username, password))) {
      this.ctx.session[sessionUserProperty] = { [userProperty]: username };
      this.ctx.rotateCsrfSecret();
      return {
        success: true,
        message: 'OK',
      };
    }
    this.ctx.status = 401;
  }

  @Post('/get_user', { middleware: [LocalPassportMiddleware] })
  @ApiResponse({
    status: 200,
    description: 'user info',
    type: UserInfoDTO,
  })
  async getUser() {
    return this.ctx.state.user;
  }
}
