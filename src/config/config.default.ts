import { MidwayConfig } from '@midwayjs/core';

export default {
  // use for cookie sign key, should change to your own and keep security
  keys: ['z*e%,Fy$bzdK1K_'],
  koa: {
    port: 7001,
  },
  midwayLogger: {
    default: {
      level: 'info',
      consoleLevel: 'info',
    },
  },
  session: {
    maxAge: 'session',
  },
  security: {
    csrf: {
      useSession: true,
    },
  },
  passport: {
    session: true,
  },
  typeorm: {
    dataSource: {
      default: {
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        database: 'midwayfull',
        username: 'root',
        password: '1Qaz@wsx',
        synchronize: false,
        logging: true,
        entities: ['**/entities/*.entity{.ts,.js}'],
      },
    },
  },
  vitessr: {
    disabled: process.env._DISABLED_START_FRONT_ !== undefined,
    prefix: '/',
    root: 'web',
    prod: {
      dist: 'dist',
      client: 'client',
    },
    dev: {
      devServerOpts: {
        server: {
          middlewareMode: true,
        },
        appType: 'custom',
      },
    },
    staticOpts: { index: false },
  },
  clientApi: {
    bin: 'scripts/APIGenerator.cjs',
    prefix: 'web/.api/docs',
    document: 'apistore.json',
    generatorKey: 'v1.0',
    spaces: 2,
    enable: process.env.ENVNODE !== 'production',
  },
  swagger: {
    title: 'MidwayFull',
    description: 'MidwayFull API document',
    version: '1.0.1',
  },
} as MidwayConfig;
