import { ApiProperty } from '@midwayjs/swagger';
import { Rule, RuleType } from '@midwayjs/validate';

export class LoginFormDTO {
  @ApiProperty({
    description: 'user id',
  })
  @Rule(RuleType.string().required())
  username: string;

  @ApiProperty({
    description: 'password',
  })
  @Rule(RuleType.string().required())
  password: string;
}
