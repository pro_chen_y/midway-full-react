import { ApiProperty } from '@midwayjs/swagger';
import { Rule, RuleType } from '@midwayjs/validate';

export class UserInfoDTO {
  @ApiProperty({
    description: 'user id',
  })
  @Rule(RuleType.string().required())
  userId: string;

  @ApiProperty({
    description: 'user name',
  })
  @Rule(RuleType.string().required())
  userName: string;

  @ApiProperty({
    description: 'email',
  })
  @Rule(RuleType.string().required())
  email: string;
}
