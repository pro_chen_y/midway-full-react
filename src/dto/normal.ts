import { ApiProperty } from '@midwayjs/swagger';
import { Rule, RuleType } from '@midwayjs/validate';

export class NormalDTO {
  @ApiProperty({
    description: 'whether succeed',
  })
  @Rule(RuleType.string().required())
  success: boolean;

  @ApiProperty({
    description: 'a message',
  })
  @Rule(RuleType.string().required())
  message: string;
}
