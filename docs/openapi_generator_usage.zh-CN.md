## 使用 OpenAPI 生成器

[安装 java8](https://zhuanlan.zhihu.com/p/116020283)
[安装 openapi-generator_cli](https://openapi-generator.tech/docs/installation)


### 配置 openapitools.json
```json
{
  "$schema": "./node_modules/@openapitools/openapi-generator-cli/config.schema.json",
  // json 空格缩进数
  "spaces": 2,
  "generator-cli": {
    // 指定 openapi-generator-cli.jar 的版本（实际的运行在 java 环境中）
    "version": "5.1.1",
    // openapi-generator-cli.jar 的下载目录, 默认的目录在对应的 npm 安装目录下
    "storageDir": "web/.api/storage",
    // 这是仓库的默认设置，支持自定义私仓
    "repository": {
      "queryUrl": "https://search.maven.org/solrsearch/select?q=g:${group.id}+AND+a:${artifact.id}&core=gav&start=0&rows=200",
      "downloadUrl": "https://repo1.maven.org/maven2/${groupId}/${artifactId}/${versionName}/${artifactId}-${versionName}.jar"
    },
    "generators": {
      // 默认生成器
      "v1.0": {
        // 生成客户端代码的生成器名称, 也可以使用 typescript-fetch
        "generatorName": "typescript-axios",
        // 输出代码目录，支持同时根据多个 OpenAPI 3.0 文档生成对应的客户端代码
        "output": "./web/.api/v1.0/#{name}",
        // OpenAPI 文档位置，除了 MidwayFull 根据 Swagger 组件自动生成的文档外，还可添加任何第三方 OpenAPI 3.0 文档
        // 扫描目录下所有 json 或 yaml 文档
        "glob": "./web/.api/docs/*.{json,yaml}"
      }
    }
  }
}
```

### 手动下载 OpenAPI Generator 可用版本 (下载 openapi-generator-cli-x-x-x.jar )

**提示** 
1. `openapi-generator-cli` 在首次执行时, 会自动下载 jar 包依赖, 由于科学上网的问题, 这个下载过程非常长。
2. `openapi-generator-cli` 默认的 jar 包依赖版本为 7.2.0 或更高, 高于 java8 的 class 版本，所以即使下载成功也无法执行, 需要降低版本为 5.x.x, 项目当前设置的版本为 5.1.1 (参考 openapitools.json)。

**解决方法**
手动下载 https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/5.1.1/openapi-generator-cli-5.1.1.jar (在浏览器中地址栏输入然后回车), 然后将对应的 jar 文件名修改为 5.1.1.jar, 添加到 web/.api/storage 目录下, 然后重新启动项目即可。

### ClientAPIGenerator-service 与客户端调用示例

**dto**

```ts
//~ src/dto/userInfo.ts
import { ApiProperty } from '@midwayjs/swagger';
import { Rule, RuleType } from '@midwayjs/validate';

export class UserInfoDTO {
  @ApiProperty({
    description: 'user id',
  })
  @Rule(RuleType.string().required())
  userId: string;

  @ApiProperty({
    description: 'user name',
  })
  @Rule(RuleType.string().required())
  userName: string;

  @ApiProperty({
    description: 'email',
  })
  @Rule(RuleType.string().required())
  email: string;
}
```

**controller**

```ts
//~ src/controller/auth.controller.ts
// ... 
  @Post('/login')
  @ApiBody({ description: 'login form' })
  @ApiResponse({
    status: 200,
    type: NormalDTO,
  })
  async login(@Body() userDto: LoginFormDTO) {
    const { username, password } = userDto;
    const { sessionUserProperty, userProperty } = this.passConfig;
    delete this.ctx.session[sessionUserProperty];

    if (true === (await this.userService.verifyUser(username, password))) {
      this.ctx.session[sessionUserProperty] = { [userProperty]: username };
      this.ctx.rotateCsrfSecret();
      return {
        success: true,
        message: 'OK',
      };
    }
    this.ctx.status = 401;
  }

  @Post('/get_user', { middleware: [LocalPassportMiddleware] })
  @ApiResponse({
    status: 200,
    description: 'user info',
    type: UserInfoDTO,
  })
  async getUser() {
    return this.ctx.state.user;
  }
// ...
```


```ts
//~ src/service/clientAPIGenerator.service.ts
// ...
Provide()
@Singleton()
export class ClientAPIGenerator extends SwaggerExplorer {
  @Config('clientApi')
  private clientApiConfig: ClientApiOptions;

  @App()
  private app: Application;

  @Inject()
  private configService: MidwayConfigService;

  @Logger()
  private logger: ILogger;

  async generate() {
    if (this.clientApiConfig?.enable) {
      let globalPrefix =
        this.configService.getConfiguration('globalPrefix') ||
        this.configService.getConfiguration('koa.globalPrefix');

      if (globalPrefix) {
        if (!/^\//.test(globalPrefix)) {
          globalPrefix = '/' + globalPrefix;
        }

        this.addGlobalPrefix(globalPrefix);
      }

      this.scanApp();

      const {
        prefix = 'web/.api/docs',
        generatorKey,
        document = 'apistore',
        spaces = 2,
      } = this.clientApiConfig ?? {};

      // 生成 openapi 3.x 文档 => 'web/.api/docs/apistore.json'
      const apiFilePath = resolve(this.app.getAppDir(), prefix, document);
      writeFileSync(
        apiFilePath,
        JSON.stringify(this.getData(), null, spaces),
        'utf-8'
      );

      this.logger.info(`written to file ${apiFilePath}`);

      // 生成 openapi 客户端代码
      await apiGenCode(prefix, generatorKey, true);
    }
  }
}
```
**client: login page**

```tsx
//~ web/pages/login/+Page.tsx
// ...

// 引用已生成的客户端 API 模块 
import { AuthApi, getInstanceForOpenAPI } from '~/apis/.apistore'
// ...

function LoginForm() {
  const { csrfToken } = usePageContext()
  const [api, contextHolder] = notification.useNotification()

  const onFinish = async (values: any) => {
    let { username, password } = values
    password = CryptoJS.enc.Base64.stringify(CryptoJS.SHA256(password))
    try {
      // 调用 API 模块
      const api = getInstanceForOpenAPI(AuthApi)
      await api.authcontrollerLogin({ username, password }, csrfToken)
      // ---
      const { pathname } = new URL(location.href)
      if (pathname === '/login') {
        window.location.replace('/')
      } else {
        window.location.reload()
      }
    } catch (e) {
      api.error({
        message: '登录失败',
        description: '用户名或密码错误！',
        placement: 'topRight'
      })
    }
  }

  // Return ...
}
// ...
```
