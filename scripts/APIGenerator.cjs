#!/usr/bin/env node

/* eslint-disable */
(function () {
  const yargs = require('yargs/yargs');
  const { spawn } = require('node:child_process');
  const fs = require('fs');
  const path = require('path');

  const COLOR_RESET = '\x1b[0m';
  const COLOR_RED = '\x1b[31m';
  const COLOR_GREEN = '\x1b[32m';
  // const COLOR_YELLOW = '\x1b[33m';
  const COLOR_BLUE = '\x1b[34m';

  function generateApis(apidir, genkey) {
    let apigendir = path.join(apidir, genkey);

    const dir = path.join(process.cwd(), apigendir);
    const root = path.join(
      process.cwd(),
      apigendir.replace(/^[.]*[\\\/]/, '').split(/[\\|\/]/)[0]
    );

    if (fs.existsSync(dir)) {
      const files = fs.readdirSync(dir, { withFileTypes: true }) || [];
      files
        .filter(d => d.isDirectory())
        .forEach(d => {
          const importPath = path
            .join(d.path, d.name)
            .replace(root, '~')
            .replace(/\\/g, '/');
          const output = path.join(
            root,
            'apis',
            d.name === 'apistore' ? '.apistore.ts' : `${d.name}.apistore.ts`
          );
          fs.writeFileSync(
            output,
            `export * from '${importPath}'\n\nexport * from './openapitools'\n`
          );
          console.log(`${COLOR_GREEN}apis: ${output}${COLOR_RESET}`);
        });
    } else {
      throw new Error('API directory not found');
    }
  }

  function verifydirs(apidir) {
    const _apidir = path.join(process.cwd(), apidir);
    if (!fs.existsSync(_apidir)) {
      console.error(
        `${COLOR_RED}Directory '${_apidir}' not found ${COLOR_RESET}`
      );
      process.exit(2);
    }

    const apidocs = path.join(_apidir, 'docs');
    if (!fs.existsSync(apidocs)) {
      console.error(
        `${COLOR_RED}Directory '${apidocs}' not found ${COLOR_RESET}`
      );
      process.exit(2);
    }

    const apistorage = path.join(_apidir, 'storage');
    if (!fs.existsSync(apistorage)) {
      console.error(
        `${COLOR_RED}Directory '${apistorage}' not found ${COLOR_RESET}`
      );
      process.exit(2);
    }
  }

  const argv = yargs(process.argv.slice(2))
    .usage('Usage: $0 -d [string] -k [string] -s [boolean]')
    .version('1.0.1')
    .alias('d', 'apidir')
    .describe('d', 'API output directory')
    .alias('k', 'genkey')
    .describe('k', 'Generator key values, reference ./openapitools.json')
    .demandOption(['d', 'k'])
    .parse();

  const { apidir, genkey } = argv;

  // 检查 API 目录结构
  verifydirs(apidir);

  // 执行 openapi-generator-cli 
  const args = ['generate', '--generator-key', genkey];
  const cp = spawn('openapi-generator-cli', args, {
    shell: true,
    cwd: process.cwd(),
  });

  cp.stdout.on('data', chunk => {
    process.stdout.write(chunk);
  });

  cp.on('exit', (code, signal) => {
    if (code !== 0) {
      console.error(
        `${COLOR_RED}Please check the parameters or try to install openapi-generator-cli.`,
        `\n${COLOR_BLUE}Example: npm i -g openapi-generator-cli`,
        `\nReference: https://openapi-generator.tech/docs/installation${COLOR_RESET}`
      );
      process.exit(code);
    } else {
      console.info(
        `${COLOR_GREEN}Client API generated, path: ${apidir}/${genkey}${COLOR_RESET}`
      );

      try {
        generateApis(apidir, genkey);
      } catch (e) {
        console.error(`${COLOR_RED}${e.stack}${COLOR_RESET}`);
        process.exit(1);
      }
    }
  });
})();
