export { getInstanceForOpenAPI }

import { AxiosInstance } from 'axios'

const MF_BASE_URL = import.meta.env.MF_BASE_URL

function getInstanceForOpenAPI<T>(
  cls: new (...args: any) => T,
  config?: any,
  axios?: AxiosInstance
): T {
  return new cls(config, MF_BASE_URL, axios)
}
