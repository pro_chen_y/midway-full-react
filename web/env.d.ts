/// <reference types="vite-plugin-svgr/client" />
/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly MF_BASE_URL: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}


